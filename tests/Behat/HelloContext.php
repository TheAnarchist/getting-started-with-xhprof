<?php declare(strict_types=1);

namespace App\Tests\Behat;

use Behat\Behat\Context\Context;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

final class HelloContext implements Context
{
    /** @var KernelInterface */
    private $kernel;

    /** @var Response|null */
    private $response;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When a hello scenario sends a request to :path
     *
     * @param string $path
     * @throws \Exception
     */
    public function aDemoScenarioSendsARequestTo(string $path): void
    {
        $this->response = $this->kernel->handle(Request::create($path, 'GET'));
    }

    /**
     * @Then the response should be received
     */
    public function theResponseShouldBeReceived(): void
    {
        if ($this->response === null) {
            throw new \RuntimeException('No response received');
        }
    }

    /**
     * @Then the response is JSON
     */
    public function theResponseIsJson(): void
    {
        if (!$this->response instanceof JsonResponse) {
            throw new \RuntimeException('The response isn\'t JSON');
        }
    }

    /**
     * @Then the response has :key with :value
     *
     * @param string $key
     * @param string $value
     * @throws \JsonException
     */
    public function theResponseHasKeyWithValue(string $key, string $value): void
    {
        $body = json_decode($this->response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if (!array_key_exists($key, $body)) {
            throw new \RuntimeException('The response has no key: ' . $key);
        }

        if ($body[$key] !== $value) {
            throw new \RuntimeException('The expected value is wrong, the received value: ' . $value);
        }
    }
}
