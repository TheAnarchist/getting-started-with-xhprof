<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class HelloController extends AbstractController
{
    /**
     * @Route("/hello/{name<\w+>}", name="hello")
     *
     * @param string $name
     * @return JsonResponse
     */
    public function index(string $name = 'World'): JsonResponse
    {
        return $this->json([
            'salutation' => sprintf('Hello, %s!', $name),
        ]);
    }
}
