<?php

use App\Kernel;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;
use Xhgui\Profiler\Profiler;

require dirname(__DIR__).'/vendor/autoload.php';

(new Dotenv())->bootEnv(dirname(__DIR__).'/.env');

$isXhGuiEnabled = getenv('XHGUI_ENABLED') === 'true';
if ($isXhGuiEnabled) {
    $config = require dirname(__DIR__).'/xhgui.config.php';
    $profiler = new Profiler($config);
    $profiler->start();
}

if ($_SERVER['APP_DEBUG']) {
    umask(0000);
    Debug::enable();
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);

if ($isXhGuiEnabled) {
    $profiler_data = $profiler->disable();
    $profiler->save($profiler_data);
}
